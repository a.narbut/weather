import React from 'react';
import { QueryClientProvider } from 'react-query';
import { CurrentWeather } from './components/CurrentWeather';
import { Filter } from './components/Filter';
import { Forecast } from './components/Forecast';
import { Head } from './components/Head';
import { client } from './hooks/client';
import { SelectedDayProvider } from './lib/weatherContext';

export const App = () => (
    <QueryClientProvider client={client}>
        <SelectedDayProvider>
            <main>
                <Filter />
                <Head />
                <CurrentWeather />
                <Forecast />
            </main>
        </SelectedDayProvider>
    </QueryClientProvider>
);
