export interface ForecastModel {
    id: string;
    rainProbability: number;
    humidity: number;
    date: Date;
    temperature: number;
    type: string;
}
