import axios from 'axios';
import { ForecastModel } from '../types/ForecastModel';

const WEATHER_API_URL = process.env.REACT_APP_WEATHER_API_URL;

export const api = {
    async getForecastByCity(): Promise<ForecastModel[]> {
        const { data } = await axios(`${WEATHER_API_URL}/forecast?city=Kiev`);

        return data.data.map((x: any) => {
            const mapResult: ForecastModel = {
                id: x.id,
                rainProbability: x.values.cloudCover,
                humidity: x.values.humidity,
                date: new Date(x.date),
                temperature: x.values.temperature,
                type: x.values.weather,
            };

            return mapResult;
        }).slice(0, 7);
    },

    // async getCities() {
    //     const { data } = await axios(`${WEATHER_API_URL}/cities`, { method: 'GET' });
    //     return data;
    // }
};
