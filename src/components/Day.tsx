import { useContext } from 'react';
import { getWeekDay } from "../helpers/getWeekday";
import { SelectedDayContext } from '../lib/weatherContext';
import { ForecastModel } from '../types/ForecastModel';

interface DayProps {
    selected: boolean;
    forecast: ForecastModel;
}

export const Day = (props: DayProps) => {
    const [_, setCurrentDay] = useContext(SelectedDayContext);

    return (
        <div className={`day ${props.forecast.type} ${props.selected ? 'selected' : ''}`} onClick={() => setCurrentDay(props.forecast)}>
            <p>{getWeekDay(props.forecast.date)}</p>
            <span>{props.forecast.temperature}</span>
        </div>
    );
};