import { useContext } from "react";
import { getDateString } from "../helpers/getDateString";
import { getWeekDay } from "../helpers/getWeekday";
import { SelectedDayContext } from "../lib/weatherContext";

export const Head = () => {
    const [selectedDay, _] = useContext(SelectedDayContext);

    if (!selectedDay) {
        return null;
    }

    return (
        <div className="head">
            <div className="icon rainy"></div>
            <div className="current-date">
                <p>{getWeekDay(selectedDay.date)}</p>
                <span>{getDateString(selectedDay.date)}</span>
            </div>
        </div>
    );
};