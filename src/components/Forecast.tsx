import { useContext, useEffect } from 'react';
import { useFetchForecast } from '../hooks/useFetchForecast';
import { SelectedDayContext } from '../lib/weatherContext';
import { Day } from './Day';

export const Forecast = () => {
    const fetchForecastQ = useFetchForecast();
    const [selectedDay, setSelectedDay] = useContext(SelectedDayContext);

    useEffect(() => {
        if (fetchForecastQ.data) {
            setSelectedDay(fetchForecastQ.data[0]);
        }
    }, [fetchForecastQ.data]);

    return (
        <div className="forecast">
            {fetchForecastQ.data?.map((x) => (
                <Day key={x.id} forecast={x} selected={selectedDay?.date === x.date} />
            ))}
        </div>
    );
};
