import { useContext } from "react";
import { SelectedDayContext } from "../lib/weatherContext";

export const CurrentWeather = () => {
    const [selectedDay, _] = useContext(SelectedDayContext);

    return (
        <div className="current-weather">
            <p className="temperature">{selectedDay?.temperature}</p>
            <p className="meta">
                <span className="rainy">%{selectedDay?.rainProbability}</span>
                <span className="humidity">%{selectedDay?.humidity}</span>
            </p>
        </div>
    );
};