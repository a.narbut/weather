import { createContext, useState } from 'react';
import { useFetchForecast } from '../hooks/useFetchForecast';
import { ForecastModel } from '../types/ForecastModel';

export const SelectedDayContext = createContext<[ForecastModel | null, React.Dispatch<React.SetStateAction<ForecastModel | null>>]>([
    null,
    () => 0,
]);

export const SelectedDayProvider: React.FC = props => {
    const state = useState<ForecastModel | null>(useFetchForecast().data?.[0] ?? null);
    return <SelectedDayContext.Provider value={state}>{props.children}</SelectedDayContext.Provider>;
};
