import { useQuery, UseQueryResult } from 'react-query';
import { api } from '../api';
import { ForecastModel } from '../types/ForecastModel';

export const useFetchForecast = (): UseQueryResult<ForecastModel[]> => {
    const query = useQuery<ForecastModel[], unknown>('forecast', api.getForecastByCity);

    return query;
};
